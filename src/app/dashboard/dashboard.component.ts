import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
@Input() socialUser:any;
isLoggedin: boolean;
loginType:string = '';
gloginType:string = '';
  constructor(private socialAuthService: SocialAuthService, private router:Router) { }

  ngOnInit(): void {
    console.log(this.socialUser); 
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      this.isLoggedin = (user != null);
      console.log(this.socialUser);
      this.loginType = FacebookLoginProvider.PROVIDER_ID; 
      this.gloginType = GoogleLoginProvider.PROVIDER_ID; 

    });
  }
  
 

}
